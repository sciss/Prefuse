lazy val baseName       = "Prefuse"
lazy val baseNameL      = baseName.toLowerCase
lazy val projectVersion = "1.0.3"

// - generate debugging symbols
// - compile to 1.8 compatible class files
// - source adheres to Java 1.8 API
lazy val commonJavaOptions = Seq("-source", "1.8")

lazy val fullDescr = "A toolkit for building interactive information visualization applications"

// sonatype plugin requires that these are in global
ThisBuild / version       := projectVersion
ThisBuild / organization  := "de.sciss"
ThisBuild / versionScheme := Some("pvp")

lazy val commonSettings = Seq(
//  version           := projectVersion,
//  organization      := "de.sciss",
  scalaVersion      := "2.13.8",  // not used
  homepage          := Some(url(s"https://github.com/Sciss/$baseName")),
  licenses          := Seq("BSD License" -> url("https://raw.githubusercontent.com/Sciss/Prefuse/main/licenses/Prefuse-License.txt")),
  crossPaths        := false,   // this is just a Java project
  autoScalaLibrary  := false,   // this is just a Java project
  javacOptions      := commonJavaOptions ++ Seq("-target", "1.8", "-g", "-Xlint:deprecation" /*, "-Xlint:unchecked" */),
  doc / javacOptions := commonJavaOptions,  // cf. sbt issue #355
  // ---- publishing to Maven Central ----
  publishMavenStyle := true,
  publishTo := {
    Some(if (isSnapshot.value)
      "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
    else
      "Sonatype Releases"  at "https://oss.sonatype.org/service/local/staging/deploy/maven2"
    )
  },
  Test / publishArtifact := false,
  Test / parallelExecution := false,
  pomIncludeRepository := { _ => false },
  pomExtra := {
  <scm>
    <url>git@github.com:Sciss/{baseName}.git</url>
    <connection>scm:git:git@github.com:Sciss/{baseName}.git</connection>
  </scm>
    <developers>
      <developer>
        <id>jheer</id>
        <name>Jeffrey Heer</name>
        <url>http://homes.cs.washington.edu/~jheer/</url>
      </developer>
      <developer>
        <id>sciss</id>
        <name>Hanns Holger Rutz</name>
        <url>http://www.sciss.de</url>
      </developer>
      <developer>
        <id>alex-rind</id>
        <name>Alexander Rind</name>
        <url>http://alex.timebench.org</url>
      </developer>
    </developers>
  }
)

// ---- dependencies ----

lazy val deps = new {
  val core = new {
    val lucene          = "1.4.3"
  }
  val test = new {
    val junitInterface  = "0.13.3"
    val mysql           = "5.1.49"
  }
}

// ---- projects ----

lazy val full = project.in(file("."))
  .aggregate(core, demos)
  .dependsOn(core, demos)
  .settings(commonSettings)
  .settings(
    name := baseName,
    description := fullDescr,
    // publishArtifact in (Compile, packageBin) := false, // there are no binaries
    // publishArtifact in (Compile, packageDoc) := false, // there are no javadocs
    // publishArtifact in (Compile, packageSrc) := false  // there are no sources
    packagedArtifacts := Map.empty
  )

lazy val core = project.in(file("core"))
  .settings(commonSettings)
  .settings(
    name        := s"$baseName-core",
    description := fullDescr,
    testOptions += Tests.Argument(TestFrameworks.JUnit),  // cf. https://github.com/michalsi/sbt-java-tests/blob/master/built.sbt
    libraryDependencies ++= Seq(
      "lucene"          % "lucene"               % deps.core.lucene,
      "com.github.sbt"  % "junit-interface"      % deps.test.junitInterface % Test,
//      "junit"           % "junit"                % "4.12"   % Test,
      "mysql"           % "mysql-connector-java" % deps.test.mysql % Test
    )
  )

lazy val demos = project.in(file("demos"))
  .dependsOn(core)
  .settings(commonSettings)
  .settings(
    name        := s"$baseName-demos",
    description := "Demo applications for the Prefuse graph visualization toolkit",
    Compile / unmanagedResourceDirectories += baseDirectory.value / ".." / "sample_data",
    Compile / run / mainClass := Some("prefuse.demos.Demos")
  )

